<?php

if (!defined('IN_IA')) {
	exit('Access Denied');
}

class List_EweiShopV2Page extends PluginMobilePage
{
	
function sub_str($sourcestr, $cutlength) {
	$returnstr = '';//待返回字符串
	$i = 0;
	$n = 0;
	$str_length = strlen ( $sourcestr ); //字符串的字节数 
	while ( ($n < $cutlength) and ($i <= $str_length) ) {
		$temp_str = substr ( $sourcestr, $i, 1 );
		$ascnum = Ord ( $temp_str ); //得到字符串中第$i位字符的ascii码 
		if ($ascnum >= 224) {//如果ASCII位高与224，
			$returnstr = $returnstr . substr ( $sourcestr, $i, 3 ); //根据UTF-8编码规范，将3个连续的字符计为单个字符  
			$i = $i + 3; //实际Byte计为3
			$n ++; //字串长度计1
		} elseif ($ascnum >= 192){ //如果ASCII位高与192，
			$returnstr = $returnstr . substr ( $sourcestr, $i, 2 ); //根据UTF-8编码规范，将2个连续的字符计为单个字符 
			$i = $i + 2; //实际Byte计为2
			$n ++; //字串长度计1
		} elseif ($ascnum >= 65 && $ascnum <= 90) {//如果是大写字母，
			$returnstr = $returnstr . substr ( $sourcestr, $i, 1 );
			$i = $i + 1; //实际的Byte数仍计1个
			$n ++; //但考虑整体美观，大写字母计成一个高位字符
		}elseif ($ascnum >= 97 && $ascnum <= 122) {
			$returnstr = $returnstr . substr ( $sourcestr, $i, 1 );
			$i = $i + 1; //实际的Byte数仍计1个
			$n ++; //但考虑整体美观，大写字母计成一个高位字符
		} else {//其他情况下，半角标点符号，
			$returnstr = $returnstr . substr ( $sourcestr, $i, 1 );
			$i = $i + 1; 
			$n = $n + 0.5; 
		}
	}
	return $returnstr;
}


	public function main()
	{
		global $_W;
		global $_GPC;
		$openid = $_W['openid'];
		$article_sys = pdo_fetch('select * from' . tablename('ewei_shop_article_sys') . 'where uniacid=:uniacid', array(':uniacid' => $_W['uniacid']));
		$article_sys['article_image'] = tomedia($article_sys['article_image']);
		$categorys = pdo_fetchall('SELECT * FROM ' . tablename('ewei_shop_article_category') . ' WHERE isshow=1 and uniacid=:uniacid order by displayorder desc ', array(':uniacid' => $_W['uniacid']));
			$arts=array(array());
			foreach ($categorys as $k=> $v){
				$articles = pdo_fetchall('SELECT a.id,g.title,a.article_linkurl,g.thumb,g.marketprice, a.article_content,g.id gid,a.article_title, a.resp_img, a.article_rule_credit, a.article_rule_money, a.resp_desc, a.article_category FROM ' . tablename('ewei_shop_article') . ' a  left join ' .tablename('ewei_shop_goods'). ' g on g.id=a.article_shop WHERE a.article_state=1 and article_visit=0  and a.uniacid= :uniacid order by a.displayorder desc, a.article_date desc', array(':uniacid' => $_W['uniacid']));
				foreach ($articles as $kk=>$vv){
					//$lenth = strlen($vv['article_content']);
					//if($lenth>=180)
					//	$str = substr($str,0,10)."**";
					//$articles[$kk]['article_content']=$this->cutstr($vv['article_content'],180)."...";
					$articles[$kk]['article_content']=$this->sub_str(strip_tags($vv['article_content']),1000);
				}
				$arts[$v['id']]=$articles;
			}
		if ($article_sys['article_temp'] == 2) {
			$categorys = pdo_fetchall('SELECT * FROM ' . tablename('ewei_shop_article_category') . ' WHERE isshow=1 and uniacid=:uniacid and isshow=1 order by displayorder desc ', array(':uniacid' => $_W['uniacid']));
			$arts=array(array());
			foreach ($categorys as $v){
				$articles = pdo_fetchall('SELECT a.id, a.article_title, a.resp_img, a.article_rule_credit, a.article_rule_money, a.resp_desc, a.article_category FROM ' . tablename('ewei_shop_article') . ' a  ' . ' WHERE a.article_state=1 and article_visit=0  and a.uniacid= :uniacid order by a.displayorder desc, a.article_date desc', array(':uniacid' => $_W['uniacid']));
				// $arts[$v['id']]=$articles;
			}
		}
	 

		include $this->template();
	}

	public function getlist()
	{
		global $_W;
		global $_GPC;
		$page = intval($_GPC['page']);
		$article_sys = pdo_fetch('select * from' . tablename('ewei_shop_article_sys') . 'where uniacid=:uniacid', array(':uniacid' => $_W['uniacid']));
		$article_sys['article_image'] = tomedia($article_sys['article_image']);
		$pindex = max(1, $page);
		$psize = empty($article_sys['article_shownum']) ? '20' : $article_sys['article_shownum'];

		if ($article_sys['article_temp'] == 0) {
			$articles = pdo_fetchall('SELECT a.id, a.article_title, a.resp_img, a.article_rule_credit, a.article_rule_money, a.resp_desc, a.article_category FROM ' . tablename('ewei_shop_article') . ' a left join ' . tablename('ewei_shop_article_category') . ' c on c.id=a.article_category  WHERE a.article_state=1 and article_visit=0 and c.isshow=1 and a.uniacid= :uniacid order by a.displayorder desc, a.article_date desc LIMIT ' . ($pindex - 1) * $psize . ',' . $psize, array(':uniacid' => $_W['uniacid']));
		}
		else if ($article_sys['article_temp'] == 1) {
			$articles = pdo_fetchall('SELECT distinct article_date_v FROM ' . tablename('ewei_shop_article') . ' a left join ' . tablename('ewei_shop_article_category') . ' c on c.id=a.article_category WHERE a.article_state=1 and c.isshow=1 and a.uniacid=:uniacid order by a.article_date_v desc limit ' . (($pindex - 1) * $psize . ',' . $psize), array(':uniacid' => $_W['uniacid']), 'article_date_v');

			foreach ($articles as &$a) {
				$a['articles'] = pdo_fetchall('SELECT id,article_title,article_date_v,resp_img,resp_desc,article_date_v,resp_desc,article_category FROM ' . tablename('ewei_shop_article') . ' WHERE article_state=1 and uniacid=:uniacid and article_date_v=:article_date_v order by article_date desc ', array(':uniacid' => $_W['uniacid'], ':article_date_v' => $a['article_date_v']));
			}

			unset($a);
		}
		else {
			if ($article_sys['article_temp'] == 2) {
				$cate = intval($_GPC['cateid']);
				$where = '';

				if (0 < $cate) {
					$where = ' and article_category=' . $cate . ' ';
				}

				$articles = pdo_fetchall('SELECT a.id, a.article_title, a.resp_img, a.article_rule_credit, a.article_rule_money, a.article_author, a.article_date_v, a.resp_desc, a.article_category FROM ' . tablename('ewei_shop_article') . ' a left join ' . tablename('ewei_shop_article_category') . ' c on c.id=a.article_category WHERE a.article_state=1 and c.isshow=1 and a.uniacid=:uniacid ' . $where . ' order by a.displayorder desc, a.article_date_v desc limit ' . (($pindex - 1) * $psize . ',' . $psize), array(':uniacid' => $_W['uniacid']));
			}
		}

		if (!empty($articles)) {
			include $this->template('article/list_tpl');
		}
	}
}

?>
